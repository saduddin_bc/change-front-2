<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NEISA | PROCESS TRACKING</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo e(url('')); ?>/pages/tsel.jpg" rel="icon" type="image/png">
    <!-- Font Awesome -->
    <!--  -->
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/dist/css/adminlte.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- *** START TAMBAH IMPORT FONTAWESOME IYON *** -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('')); ?>/plugins/fontawesome-free-5.6.3-web/css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('')); ?>/plugins/fontawesome-free-5.6.3-web/css/solid.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('')); ?>/plugins/fontawesome-free-5.6.3-web/css/brands.css">
    <!-- *** END TAMBAH IMPORT FONTAWESOME IYON *** -->

    <style>
        /* ** START TAMBAH CSS IYON** */
        .dropClass {
            /* display: none; */
            visibility: hidden;
            opacity: 0;
            transition: 
            all .5s ease;
            background-color: transparent;
            min-width: 160px;
            overflow: auto;
            z-index: 1;
            height: 0;
            /* margin-bottom: -5%; */
            
        }
        
        .dropClass a {
            color: black;
            padding: 12px 16px;
            /* text-decoration: none; */
            display: block;
        }
        
        .dropClass a:hover {background-color: rgba(255,255,255,.1);}
        .show {
            /* display: block; */
            visibility: visible;
            opacity: 1;
            height: auto;
            padding: 0.5rem 1rem;
            background-color: rgba(255,255,255,.3);
            margin-bottom: 1.5%;
            border-radius: 3%;
        }

        .putar {
            transform: rotate(90deg);
            transition: all .5s ease;
        }

        .brand-image {
            line-height: .8;
            max-height: 53px;
            width: auto;
            margin-left: 0.7rem;
            margin-right: .5rem;
            margin-top: -3px;
            float: none;
            opacity: .9;
        }

        .backgroundImg {
            width: auto;
            height: 100%;
            opacity: 1;
            position: absolute;
        }

        .backgroundImg2 {
            position: fixed;
            width: 100%;
            max-height: 56px;
            margin-left: -2%;
            opacity: 1;
        }

        .nav-item:hover {
            background-color: rgba(255,255,255,.3);
            border-radius: 5%;
            transition: all .2s ease;
        }

        .active {
            background-color: rgba(243, 255, 226, .8) !important;
            color: #343a40 !important;
            font-weight: 600;
        }

        .berisik {
        min-height:500px !important
        }

        .headerBox {
            margin-bottom: -5% !important;
            /* z-index: 1; */
            /* opacity: 1; */
        }

        /* .boksBody1 {
            background-color:#1caffd;
        } */

        .boksHead1 {
            background-color: #1aaffd;
        }

        .boksHead2 {
            background-color: #40dcdb;
        }

        .boksHead3 {
            background-color: #30d0dc;
        }

        .boksHead4 {
            background-color: #815586;
        }

        .boksHead5 {
            background-color: #a25c80;
        }

        .boksHead6 {
            background-color: #cb657b;
        }

        .boksHead {
            font-size: 17px;
            box-shadow: 0 3px 1px 0 rgba(0, 0, 0, 0.2), 0 1px 0px 0 rgba(0, 0, 0, 0.19);
            padding: 10px;
            font-weight: 500;
        }

        .boksBody {
            height: 90px;
            background-color: #343a40;
            border-radius: 0;
        }

        @media (min-width: 992px) {
            .huge {
                margin-top: -5% !important;
        }
        }

        .headAktif {
            background-color: #74c7b7;
        }

        .headDeaktif {
            background-color: #fe786f;
        }

        /* .ikons {
            opacity: 1;
        } */

        /* ** END TAMBAH IYON** */
    </style>

</head>

<body class="hold-transition sidebar-mini" style="background: #f4f6f9; color: white;">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand bg-dark" style="margin-left: 250px; position:fixed; width:100%;">
            <img src="<?php echo e(url('')); ?>/dist/img/wall5.jpg" class="backgroundImg2" style="position: fixed;width: 100%;">
            <!--  -->
            <!-- Left navbar links -->
            <ul class="navbar-nav" style="z-index: 999;">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars nav-button" style="color:white;"></i></a>
                </li>
                <li class=" navbar-brand" style="color:white; margin-left: 10%;">NEISA | PROCESS TRACKING</li>
                <li class="nav-item">
                    <a class="nav-link btn btn-lg" href="http://10.54.36.49/landingPage/" onclick="sessionStorage.clear();" style="
                        color: #343a40 !important;
                        background-color: #fff;
                        position: fixed;
                        font-size: 10px;
                        right: 1%;
                        height: auto;
                        box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                        text-transform: uppercase;
                        font-family: Roboto;
                        padding: 1%;"><i class="fa fa-sign-out-alt"></i> Log Out</a>
                </li>
            </ul>
        </nav>
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4 berisik">
            <img src="<?php echo e(url('')); ?>/dist/img/wall3.jpg" class="backgroundImg">
            <!--  -->
            <!-- Brand Logo -->
            <a href="#" class="brand-link">
                <img src="<?php echo e(url('')); ?>/dist/img/tsel-white.png"
                style="opacity: .8; float:none; widht:200px; line-height:.8; max-height:53px;margin-left:0.7rem;margin-right:.5rem;margin-top:-3px">
            </a>
            <!-- SIDEBAR -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item">
                            <a href="http://10.54.36.49/dashboard-bts-on-air/public/" class="nav-link" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-home"></i>
                                <p style="margin-left: 3px;">Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/dashboard-license" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-newspaper"></i>
                                <p style="margin-left: 3px;">License</p>
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="http://10.54.36.49/btsonair" class="nav-link " style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-broadcast-tower"></i>
                                <p style="margin-left: 3px;">BTS Status</p>
                            </a>
                        </li> -->
                        <li class="nav-item" style="cursor: pointer;">
                            <a onclick="dropDead()" class="nav-link aa dropbtn" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-angle-right" id="dropIcon"></i>
                                <p style="margin-left: 3px;">Create</p>
                            </a>
                        </li>
                        <div class="dropClass" id="dropId">
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Integrasi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin-stylo/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Rehoming</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin-dismantle/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Dismantle</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin-relocation/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Relocation</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin-swap/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Swap</p>
                                </a>
                            </li>
                        </div>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/change-front-2/public/" class="nav-link aa active" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-project-diagram"></i>
                                <p style="margin-left: 3px;">Process Tracking</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/tableList" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-table"></i>
                                <p style="margin-left: 3px;">Nodin & MoM Report</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/apk-report/index.php/ReportController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-book"></i>
                                <p style="margin-left: 3px;">Report Remedy</p>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div><!-- SIDEBAR-->
        </aside>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="margin-top:60px; min-height: 553px;">
            <nav aria-label="breadcrumb" style="margin-top: -5px;">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item" aria-current="page"><a href="<?php echo e(url('index')); ?>">Regional 1</a></li>
                    <li class="breadcrumb-item" aria-current="page"><a href="<?php echo e(url('index2')); ?>">Regional 2</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Regional 3</li>
                    <li class="breadcrumb-item" aria-current="page"><a href="<?php echo e(url('index4')); ?>">Regional 4</a></li>
                    <li class="breadcrumb-item" aria-current="page"><a href="<?php echo e(url('index5')); ?>">Regional 5</a></li>
                    <li class="breadcrumb-item" aria-current="page"><a href="<?php echo e(url('index6')); ?>">Regional 6</a></li>
                    <li class="breadcrumb-item" aria-current="page"><a href="<?php echo e(url('index7')); ?>">Regional 7</a></li>
                    <li class="breadcrumb-item" aria-current="page"><a href="<?php echo e(url('index8')); ?>">Regional 8</a></li>
                    <li class="breadcrumb-item" aria-current="page"><a href="<?php echo e(url('index9')); ?>">Regional 9</a></li>
                    <li class="breadcrumb-item" aria-current="page"><a href="<?php echo e(url('index10')); ?>">Regional 10</a></li>
                    <li class="breadcrumb-item" aria-current="page"><a href="<?php echo e(url('index11')); ?>">Regional 11</a></li>
                </ol>
            </nav>
            <section class="content">
                <div class="container-fluid">
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-sm-12 mt-3" id="deretboks">
                            <div class="row">
                                <div class="col-lg-2 col-md-2" id="bok1">
                                    <!-- small box -->
                                    <div class="small-box headerBox boksHead1 boksHead" id="dafinci_change">
                                        NEW ON AIR  
                                    </div>
                                    <div class="small-box boks boksBody">
                                        <div class="inner">
                                            <div class="col-xs-12 text-right">
                                                <i class="nav-icon fa fa-chart-line ikons" style="
                                                    font-size: 50px;
                                                    width: 100%;
                                                    text-align: left;">
                                                </i>
                                                <div class="huge" style="    
                                                    font-size: 40px;
                                                    margin-top: -24%;" >
                                                    <h2 id="dafinci" style="width: 75%;"><i class="fa fa-spinner fa-spin"></i></h2>
                                                    <h6 style="margin-top: -18%;">Site</h6>
                                                </div>
                                                <!--  -->
                                            </div>
                                            <!--  -->
                                            <div class="clearfix"></div>
                                        </div>
                                    </div><!-- BOKSBODY-->
                                </div><!-- BOK 1 -->                             
                                <!-- ./col -->
                                <div class="col-lg-2 col-md-2" id="bok1">
                                    <!-- small box -->
                                    <div class="small-box headerBox boksHead2 boksHead" id="nodin_change">
                                            NODIN
                                    </div>
                                    <div class="small-box boks boksBody">
                                        <div class="inner">
                                            <div class="col-xs-12 text-right">
                                                <i class="nav-icon fa fa-chart-line ikons" style="
                                                    font-size: 50px;
                                                    width: 100%;
                                                    text-align: left;"></i>
                                                <div class="huge" style="    
                                                    font-size: 40px;
                                                    margin-top: -24%;" >
                                                    <h2 id="nodin" style="width: 75%;"><i class="fa fa-spinner fa-spin"></i></h2>
                                                    <h6 style="margin-top: -18%;">Nodin</h6>
                                                </div>
                                                <!--  -->
                                            </div>
                                            <!--  -->
                                            <div class="clearfix"></div>
                                        </div>
                                    </div><!-- BOKSBODY-->
                                </div><!-- BOK 1 -->
                                <!-- ./col -->
                                <div class="col-lg-2 col-md-2" id="bok1">
                                    <!-- small box -->
                                    <div class="small-box headerBox boksHead3 boksHead" id="remedy_change">
                                        REMEDY
                                    </div>
                                    <div class="small-box boks boksBody">
                                        <div class="inner">
                                            <div class="col-xs-12 text-right">
                                                <i class="nav-icon fa fa-chart-line ikons" style="
                                                    font-size: 50px;
                                                    width: 100%;
                                                    text-align: left;"></i>
                                                <div class="huge" style="    
                                                    font-size: 40px;
                                                    margin-top: -24%;" >
                                                    <h2 id="remedy" style="width: 75%;"><i class="fa fa-spinner fa-spin"></i></h2>
                                                    <h6 style="margin-top: -18%;">CRQ</h6>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div><!-- BOKSBODY-->
                                    <!--  -->
                                </div><!-- BOK 1 -->
                                <!-- ./col -->
                                <div class="col-lg-2 col-md-2" id="bok1">
                                    <!-- small box -->
                                    <div class="small-box headerBox boksHead4 boksHead" id="change_add_bts">
                                        CHANGE
                                    </div>
                                    <div class="small-box boks boksBody">
                                        <div class="inner">
                                            <div class="col-xs-12 text-right">
                                                <i class="nav-icon fa fa-chart-line ikons" style="
                                                    font-size: 50px;
                                                    width: 100%;
                                                    text-align: left;"></i>
                                                <div class="huge" style="    
                                                    font-size: 40px;
                                                    margin-top: -24%;" >
                                                    <h2 id="add_bts" style="width: 75%;"><i class="fa fa-spinner fa-spin"></i></h2>
                                                    <h6 style="margin-top: -18%;">Site</h6>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div><!-- BOKSBODY-->
                                    <!--  -->
                                </div><!-- BOK 1 -->
                                <!-- ./col -->
                                <div class="col-lg-2 col-md-2" id="bok1">
                                    <!-- small box -->
                                    <div class="small-box headerBox boksHead5 boksHead" id="license_change">
                                        LICENSE
                                    </div>
                                    <div class="small-box boks boksBody">
                                        <div class="inner">
                                            <div class="col-xs-12 text-right">
                                                <i class="nav-icon fa fa-chart-line ikons" style="
                                                    font-size: 50px;
                                                    width: 100%;
                                                    text-align: left;"></i>
                                                <div class="huge" style="    
                                                    font-size: 40px;
                                                    margin-top: -24%;" >
                                                    <h2 id="license" style="width: 75%;"><i class="fa fa-spinner fa-spin"></i></h2>
                                                    <h6 style="margin-top: -18%;">Site</h6>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div><!-- BOKSBODY -->
                                    <!--  -->
                                </div><!-- BOK 1 -->
                                <div class="col-lg-2 col-md-2" id="bok1">
                                    <!-- small box -->
                                    <div class="small-box headerBox boksHead6 boksHead" id="btsonair_change">
                                            BTS ON AIR
                                        </div>
                                        <div class="small-box boks boksBody">
                                            <div class="inner">
                                                <div class="col-xs-12 text-right">
                                                    <i class="nav-icon fa fa-chart-line ikons" style="
                                                        font-size: 50px;
                                                        width: 100%;
                                                        text-align: left;"></i>
                                                    <div class="huge" style="    
                                                        font-size: 40px;
                                                        margin-top: -24%;" >
                                                        <h2 id="btsonair" style="width: 75%;"><i class="fa fa-spinner fa-spin"></i></h2>
                                                        <h6 style="margin-top: -18%;">Site</h6>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div><!-- BOKSBODY -->
                                    <!--  -->
                                </div><!-- BOK 1 -->
                            </div><!-- ROW -->
                        </div><!-- DERET BOKS -->
                    </div><!-- ROW-->
                    
                    <div class="row">
                        <!-- *** START CHART PLACEMENT NEW *** -->
                        <section class="col-lg-6" id="charthiji">
                            <div class="card">
                                <div class="card-header" style="background-color: #008080;">
                                    <h3 style="color: white;" class="card-title">
                                        <i class="fa fa-th mr-1"></i>
                                        Count of BTS On Air Total (Region)
                                    </h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-widget="collapse">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-tool" data-widget="remove">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </div>
                                </div><!-- CARD HEADER-->
                                <div class="card-body" style="background-color: #343a40;">
                                <center><label class="pull-center" style="color:white;">Total &nbsp;</label><button style="width: 12px; height: 12px; background-color: #008080; border: 0px;"></button></center>
                                    <?php echo $__env->make('top_5_chart_monly', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div><!-- CARD BODY -->
                            </div><!-- CARD -->
                        </section><!-- CHART HIJI -->
                        <section class="col-lg-6" id="chartduak">
                            <div class="card">
                                <div class="card-header" style="background-color: #008080;">
                                    <h3 style="color: white;" class="card-title">
                                        <i class="fa fa-th mr-1"></i>
                                        Count of Planned and On Air New (Region)
                                    </h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-widget="collapse">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-tool" data-widget="remove">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </div>
                                </div><!-- CARD HEADER-->
                                <div class="card-body" style="background-color: #343a40;">
                                    <center><label class="pull-center" style="color:white;">Planned &nbsp;</label><button style="width: 12px; height: 12px; background-color: #fff; border: 0px"></button>&nbsp;&nbsp;&nbsp;<label class="pull-center" style="color:white;">On Air New &nbsp;</label><button style="width: 12px; height: 12px; background-color: #008080; border: 0px;"></button></center>
                                    <?php echo $__env->make('plan_onair', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div><!-- CARD BODY -->
                            </div><!-- CARD -->
                        </section><!-- CHART DUAK -->
                    </div><!-- ROW-->
                    <!-- *** END CHART PLACEMENT NEW *** -->

                    <!-- *** START DEA LICENSE BARU *** -->
                    <div class="row">
                        <!-- <div class="col-lg-6"> -->
                        <div class="col-lg-12" id="deretboks">
                            <div class="row">
                                <div class="col-lg-3" id="bok2">
                                    <div class="small-box headerBox headAktif boksHead text-center" id="nodin">
                                        ACTIVATED 
                                        <br>
                                        CELLS
                                    </div><!-- HEADER BOKS -->
                                    <div class="small-box boksBody" style="height: 207px;">
                                        <div class="inner">
                                            <div class="col-xs-12 text-center" style="margin-top: 15%;">
                                                <!--  -->
                                                <div class="huge ccval" style="font-size:40px;" id="act_cell"><h1 style="color:white"><i class="fa fa-spinner fa-spin"></i></h1></div>
                                                <div class="huge ccll" style="font-size:20px;"><h4 style="color:white">CELLS</h4></div>
                                                <i class="nav-icon fa fa-satellite-dish ikons" style="
                                                    font-size: 50px;
                                                    /* margin-top: 30%; */
                                                    "></i>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div><!-- BOKS BODY -->
                                </div><!-- BOKS 2 -->
                                <!-- ./col -->
                                <div class="col-lg-3" id="bok2">
                                    <!-- small box -->
                                    <div class="small-box headerBox headDeaktif boksHead text-center" id="nodin">
                                        DEACTIVATED 
                                        <br>
                                        CELLS
                                    </div><!-- HEADER BOKS -->
                                    <div class="small-box boksBody" style="height: 207px;">
                                        <div class="inner">
                                            <div class="col-xs-12 text-center" style="margin-top: 15%;">
                                                <!--  -->
                                                <div class="huge ccval" style="font-size:40px;" id="dea_cell"><h1 style="color:white"><i class="fa fa-spinner fa-spin"></i></h1></div>
                                                <div class="huge ccll" style="font-size:20px;"><h4 style="color:white">CELLS</h4></div>
                                                <i class="nav-icon fa fa-satellite-dish ikons" style="
                                                    font-size: 50px;
                                                    /* margin-top: 30%; */
                                                    "></i>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div><!-- BOKS BODY -->
                                        <!--  -->
                                </div><!-- BOKS 2 -->
                                <!-- ./col -->
                                <div class="col-lg-3" id="bok2">
                                    <!-- small box -->
                                    <div class="small-box headerBox headAktif boksHead text-center" id="nodin">
                                        ACTIVATED
                                        <br> 
                                        LICENSE
                                    </div><!-- HEADER BOKS -->
                                    <div class="small-box boksBody" style="height: 207px;">
                                        <div class="inner">
                                            <div class="col-xs-12 text-center" style="margin-top: 15%;">
                                                <!--  -->
                                                <div class="huge ccval" style="font-size:40px;" id="act_license"><h1 style="color:white"><i class="fa fa-spinner fa-spin"></i></h1></div>
                                                <div class="huge ccll" style="font-size:20px;"><h4 style="color:white">LICENSE</h4></div>
                                                <i class="nav-icon fa fa-scroll ikons" style="
                                                    font-size: 50px;
                                                    /* margin-top: 30%; */
                                                    "></i>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div><!-- BOKS BODY -->
                                    <!--  -->
                                </div><!-- BOKS 2 -->
                                <!-- ./col -->
                                <div class="col-lg-3" id="bok2">
                                    <div class="small-box headerBox headDeaktif boksHead text-center" id="nodin">
                                        DEACTIVATED 
                                        <br>
                                        LICENSE
                                    </div><!-- HEADER BOKS -->
                                    <div class="small-box boksBody" style="height: 207px;">
                                        <div class="inner" >
                                            <div class="col-xs-12 text-center" style="margin-top: 15%;">
                                                <!--  -->
                                                <div class="huge ccval" style="font-size:40px;" id="dea_license"><h1 style="color:white"><i class="fa fa-spinner fa-spin"></i></h1></div>
                                                <div class="huge ccll" style="font-size:20px;"><h4 style="color:white">LICENSE</h4></div>
                                                <i class="nav-icon fa fa-scroll ikons" style="
                                                    font-size: 50px;
                                                    /* margin-top: 30%; */
                                                    "></i>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    <!--  -->
                                    </div><!-- BOKS BODY -->
                                </div><!-- BOKS 2 -->
                            </div><!-- ROW-->
                        </div><!-- DERET BOKS -->
                    </div><!-- ROW -->
                    <!-- *** END DEA LICENSE BARU *** -->

                    <!-- <section class="col-lg-6">
                        <div class="card">
                            <div class="card-header" style="background-color: #008080;">
                                <h3 style="color: white;" class="card-title">
                                    <i class="fa fa-th mr-1"></i>
                                    Count of BTS On Air Total (Region)
                                </h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-widget="remove">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body" style="background-color: #343a40;">
                                <?php echo $__env->make('top_5_chart_monly', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                        </section> -->
                        <!-- </div> -->
                </div><!-- CONTAINER-FLUID-->
            </section><!-- CONTENT -->
        </div><!-- CONTENT-WRAPPER -->

        <footer class="main-footer">
            <strong style="font-size: 12px">Copyright &copy; 2018 <a href="https://www.telkomsel.com">Telkomsel</a>.</strong>
            <div class="float-right d-none d-sm-inline-block">
                <b>CHANGE
            </div>
        </footer>
    </div><!-- WRAPPER -->
</body>
<!-- jQuery -->
<script src="<?php echo e(url('')); ?>/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src="<?php echo e(url('')); ?>/js/jquery-ui.min.js"></script>
<script>
$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo e(url('')); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo e(url('')); ?>/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo e(url('')); ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo e(url('')); ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo e(url('')); ?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo e(url('')); ?>/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?php echo e(url('')); ?>/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo e(url('')); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo e(url('')); ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo e(url('')); ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo e(url('')); ?>/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(url('')); ?>/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo e(url('')); ?>/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo e(url('')); ?>/dist/js/demo.js"></script>
<script>

function dropDead() {
    document.getElementById("dropIcon").classList.toggle('putar');
    document.getElementById("dropId").classList.toggle("show");
  }
    $("#nav-open").click(function(){
        if (document.getElementById("wrapper").style.padding == "0px") {
            $("#wrapper").css('padding-left', '150px');
            $("#sidebar-wrapper").css('width', '150px');
        }else {
            document.getElementById("wrapper").style.padding = "0";
            document.getElementById("sidebar-wrapper").style.width = "0";
        }
    });

    $(document).ready(function(){
        $.ajax({
            url:'<?php echo e(url("dafinci3")); ?>',
            type:'get',
            success: function(result){
                $('#dafinci').html(result);
                $('#dafinci_change').html('NEW ON AIR&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
            }
        });
        $.ajax({
            url:'<?php echo e(url("remedy3")); ?>',
            type:'get',
            success: function(result){
                $('#remedy').html(result);
                $('#remedy_change').html('REMEDY&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
            }
        });
        $.ajax({
            url:'<?php echo e(url("add_bts3")); ?>',
            type:'get',
            success: function(result){
                $('#add_bts').html(result);
                $('#change_add_bts').html('CHANGE&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
            }
        });
        $.ajax({
            url:'<?php echo e(url("btsonair3")); ?>',
            type:'get',
            success: function(result){
                $('#btsonair').html(result);
                $('#btsonair_change').html('BTS ON AIR&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
            }
        });
        $.ajax({
            url:'<?php echo e(url("nodin3")); ?>',
            type:'get',
            success: function(result){
                $('#nodin').html(result);
                $('#nodin_change').html('NODIN&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
            }
        });

        $.ajax({
            url:'<?php echo e(url("act_cell3")); ?>',
            type:'get',
            success: function(result){
                $('#act_cell').html('<h1 style="color:white">'+result+'</h1>');
            }
        });

        $.ajax({
            url:'<?php echo e(url("dea_cell3")); ?>',
            type:'get',
            success: function(result){
                $('#dea_cell').html('<h1 style="color:white">'+result+'</h1>');
            }
        });

        $.ajax({
            url:'<?php echo e(url("act_license3")); ?>',
            type:'get',
            success: function(result){
                $('#act_license').html('<h1 style="color:white">'+result+'</h1>');
            }
        });

        $.ajax({
            url:'<?php echo e(url("dea_license3")); ?>',
            type:'get',
            success: function(result){
                $('#dea_license').html('<h1 style="color:white">'+result+'</h1>');
            }
        });

        $.ajax({
            url:'<?php echo e(url("add_license3")); ?>',
            type:'get',
            success: function(result){
                $('#license').html(result);
                $('#license_change').html('LICENSE&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
            }
        });

    });
</script>
</html>
