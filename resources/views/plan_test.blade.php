<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <div class="row">
            <div class="card-body" style="height:180px; padding-top: 3px; ">
                <div class="chart" style="height:180px; background-color: #343a40;">
                <!-- <canvas id="myChart1"></canvas> -->
                <canvas id="myChart2"></canvas>
                </div>
            </div>
        </div>
    </body>

    <script type="text/javascript" src="{{url('')}}/js/app.js"></script>
    <!-- AdminLTE App -->
    <script src="{{url('')}}/custom/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{url('')}}/custom/js/demo.js"></script>
    <!-- jQuery -->
    <script src="{{url('')}}/custom/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <!-- <script src="custom/bootstrap/js/bootstrap.bundle.min.js"></script> -->
    <!-- ChartJS 1.0.1 -->
    <script src="{{url('')}}/custom/chartjs-old/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="{{url('')}}/custom/fastclick/fastclick.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url:'{{url("plan_remedy")}}',
            type:'get',
            success: function(result){
                console.log("result.hasil2",result);

                var chartData = {
                    labels: 
                    result.regional,
                    // ["January", "February", "March", "April", "May", "June"],
                    datasets: [
                        {
                            // fillColor: "#79D1CF",
                            // strokeColor: "#79D1CF",
                            data                : result.hasil1,
                            fillColor           : '#fff',
                            strokeColor         : '#c2c7d0',
                            pointColor          : '#fff',
                            pointStrokeColor    : '#fff',
                            pointHighlightFill  : '#fff',
                            pointHighlightStroke: '#fff',
                        },
                        {
                            // fillColor: "#c2c7d0",
                            // strokeColor: "#c2c7d0",
                            data                : result.hasil2,
                            fillColor           : '#008080',
                            strokeColor         : '#c2c7d0',
                            pointColor          : '#008080',
                            pointStrokeColor    : '#008080',
                            pointHighlightFill  : '#008080',
                            pointHighlightStroke: '#008080',
                        },
                    ]
                };

    // var ctx = document.getElementById("myChart1").getContext("2d");
    // var myLine = new Chart(ctx).Line(chartData, {
    //     showTooltips: false,
    //     onAnimationComplete: function () {

    //         var ctx = this.chart.ctx;
    //         ctx.font = this.scale.font;
    //         ctx.fillStyle = this.scale.textColor
    //         ctx.textAlign = "center";
    //         ctx.textBaseline = "bottom";

    //         this.datasets.forEach(function (dataset) {
    //             dataset.points.forEach(function (points) {
    //                 ctx.fillText(points.value, points.x, points.y - 10);
    //             });
    //         })
    //     }
    // });




// *** LIAT INI AJA
            var ctx = document.getElementById("myChart2").getContext("2d");
            var myBar = new Chart(ctx).Bar(chartData, {
                showTooltips: false,
                onAnimationComplete: function () {

                    var ctx = this.chart.ctx;
                    ctx.font = this.scale.font;
                    ctx.fillStyle = this.scale.textColor
                    ctx.textAlign = "center";
                    ctx.textBaseline = "bottom";

                    this.datasets.forEach(function (dataset) {
                        dataset.bars.forEach(function (bar) {
                            ctx.fillText(bar.value, bar.x, bar.y - 5);
                        });
                    })
                },
                    //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                    scaleBeginAtZero        : true,
                    //Boolean - Whether grid lines are shown across the chart
                    scaleShowGridLines      : true,
                    //String - Colour of the grid lines
                    scaleGridLineColor      : '#0000',
                    //Number - Width of the grid lines
                    scaleGridLineWidth      : 1,
                    scaleFontColor          :'#ffffff',
                    scaleLineColor          : '#ffffff',
                    //Boolean - Whether to show horizontal lines (except X axis)
                    scaleShowHorizontalLines: true,
                    //Boolean - Whether to show vertical lines (except Y axis)
                    scaleShowVerticalLines  : true,
                    //Boolean - If there is a stroke on each bar
                    barShowStroke           : true,
                    //Number - Pixel width of the bar stroke
                    barStrokeWidth          : 2,
                    //Number - Spacing between each of the X value sets
                    barValueSpacing         : 5,
                    //Number - Spacing between data sets within X values
                    barDatasetSpacing       : 1,
                    //String - A legend template
                    // legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
                    //Boolean - whether to make the chart responsive
                    responsive              : true,
                    maintainAspectRatio     : false,
            });
        }
    });
});

    
</script>
</html>