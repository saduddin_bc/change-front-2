<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    const URL = 'http://10.54.36.49/change2/public/';

    //btsonair 1-11
    static function add_bts()
    {
        $url = self::URL.'add_bts';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function add_bts2()
    {
        $url = self::URL.'add_bts2';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function add_bts3()
    {
        $url = self::URL.'add_bts3';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function add_bts4()
    {
        $url = self::URL.'add_bts4';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function add_bts5()
    {
        $url = self::URL.'add_bts5';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function add_bts6()
    {
        $url = self::URL.'add_bts6';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function add_bts7()
    {
        $url = self::URL.'add_bts7';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function add_bts8()
    {
        $url = self::URL.'add_bts8';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function add_bts9()
    {
        $url = self::URL.'add_bts9';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function add_bts10()
    {
        $url = self::URL.'add_bts10';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function add_bts11()
    {
        $url = self::URL.'add_bts11';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    //
    
    static function top_5_user_activity()
    {
        $url = self::URL.'top_5_user_activity';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function user_activity()
    {
        $url = self::URL.'user_activity';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function mml_command($command)
    {
        $url = self::URL.'mml_command/'.$command;
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    //active license 1-11
    static function license_activated()
    {
        $url = self::URL.'license_activated';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function license_activated2()
    {
        $url = self::URL.'license_activated2';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function license_activated3()
    {
        $url = self::URL.'license_activated3';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function license_activated4()
    {
        $url = self::URL.'license_activated4';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function license_activated5()
    {
        $url = self::URL.'license_activated5';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function license_activated6()
    {
        $url = self::URL.'license_activated6';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function license_activated7()
    {
        $url = self::URL.'license_activated7';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function license_activated8()
    {
        $url = self::URL.'license_activated8';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function license_activated9()
    {
        $url = self::URL.'license_activated9';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function license_activated10()
    {
        $url = self::URL.'license_activated10';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }    

    static function license_activated11()
    {
        $url = self::URL.'license_activated11';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    //

    //deactive cell 1-11
    static function cell_deactivated()
    {
        $url = self::URL.'cell_deactivated';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function cell_deactivated2()
    {
        $url = self::URL.'cell_deactivated2';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function cell_deactivated3()
    {
        $url = self::URL.'cell_deactivated3';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function cell_deactivated4()
    {
        $url = self::URL.'cell_deactivated4';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function cell_deactivated5()
    {
        $url = self::URL.'cell_deactivated5';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function cell_deactivated6()
    {
        $url = self::URL.'cell_deactivated6';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function cell_deactivated7()
    {
        $url = self::URL.'cell_deactivated7';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function cell_deactivated8()
    {
        $url = self::URL.'cell_deactivated8';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function cell_deactivated9()
    {
        $url = self::URL.'cell_deactivated9';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function cell_deactivated10()
    {
        $url = self::URL.'cell_deactivated10';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function cell_deactivated11()
    {
        $url = self::URL.'cell_deactivated11';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    //

    //dafinci 1-11
    static function dafinci()
    {
        $url = self::URL.'dafinci';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dafinci2()
    {
        $url = self::URL.'dafinci2';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dafinci3()
    {
        $url = self::URL.'dafinci3';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dafinci4()
    {
        $url = self::URL.'dafinci4';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dafinci5()
    {
        $url = self::URL.'dafinci5';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dafinci6()
    {
        $url = self::URL.'dafinci6';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dafinci7()
    {
        $url = self::URL.'dafinci7';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dafinci8()
    {
        $url = self::URL.'dafinci8';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dafinci9()
    {
        $url = self::URL.'dafinci9';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dafinci10()
    {
        $url = self::URL.'dafinci10';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dafinci11()
    {
        $url = self::URL.'dafinci11';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    //

    //change 1-11
    static function change()
    {
        $url = self::URL.'change';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function change2()
    {
        $url = self::URL.'change2';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function change3()
    {
        $url = self::URL.'change3';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function change4()
    {
        $url = self::URL.'change4';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function change5()
    {
        $url = self::URL.'change5';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function change6()
    {
        $url = self::URL.'change6';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function change7()
    {
        $url = self::URL.'change7';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function change8()
    {
        $url = self::URL.'change8';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function change9()
    {
        $url = self::URL.'change9';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function change10()
    {
        $url = self::URL.'change10';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function change11()
    {
        $url = self::URL.'change11';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    //

    //remedy 1-11
    static function remedy()
    {
        $url = self::URL.'remedy';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function remedy2()
    {
        $url = self::URL.'remedy2';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function remedy3()
    {
        $url = self::URL.'remedy3';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function remedy4()
    {
        $url = self::URL.'remedy4';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function remedy5()
    {
        $url = self::URL.'remedy5';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function remedy6()
    {
        $url = self::URL.'remedy6';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function remedy7()
    {
        $url = self::URL.'remedy7';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function remedy8()
    {
        $url = self::URL.'remedy8';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function remedy9()
    {
        $url = self::URL.'remedy9';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function remedy10()
    {
        $url = self::URL.'remedy10';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function remedy11()
    {
        $url = self::URL.'remedy11';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }    
    //

    //btsonair 1-11
    static function btsonair()
    {
        $url = self::URL.'btsonair';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function btsonair2()
    {
        $url = self::URL.'btsonair2';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function btsonair3()
    {
        $url = self::URL.'btsonair3';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function btsonair4()
    {
        $url = self::URL.'btsonair4';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function btsonair5()
    {
        $url = self::URL.'btsonair5';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function btsonair6()
    {
        $url = self::URL.'btsonair6';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function btsonair7()
    {
        $url = self::URL.'btsonair7';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function btsonair8()
    {
        $url = self::URL.'btsonair8';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function btsonair9()
    {
        $url = self::URL.'btsonair9';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function btsonair10()
    {
        $url = self::URL.'btsonair10';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function btsonair11()
    {
        $url = self::URL.'btsonair11';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    //

    //nodin 1-11
    static function nodin()
    {
        $url = self::URL.'nodin';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function nodin2()
    {
        $url = self::URL.'nodin2';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }    

    static function nodin3()
    {
        $url = self::URL.'nodin3';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function nodin4()
    {
        $url = self::URL.'nodin4';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function nodin5()
    {
        $url = self::URL.'nodin5';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function nodin6()
    {
        $url = self::URL.'nodin6';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function nodin7()
    {
        $url = self::URL.'nodin7';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function nodin8()
    {
        $url = self::URL.'nodin8';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function nodin9()
    {
        $url = self::URL.'nodin9';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function nodin10()
    {
        $url = self::URL.'nodin10';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function nodin11()
    {
        $url = self::URL.'nodin11';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }    
    //

    static function chart_btsonair()
    {
        $url = self::URL.'chart_btsonair';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_nodin()
    {
        $url = self::URL.'chart_nodin';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_user()
    {
        $url = self::URL.'chart_user';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function chart_dea()
    {
        $url = self::URL.'chart_dea';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    //active cell 1-11
    static function act_cell()
    {
        $url = self::URL.'act_cell';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function act_cell2()
    {
        $url = self::URL.'act_cell2';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function act_cell3()
    {
        $url = self::URL.'act_cell3';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function act_cell4()
    {
        $url = self::URL.'act_cell4';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function act_cell5()
    {
        $url = self::URL.'act_cell5';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function act_cell6()
    {
        $url = self::URL.'act_cell6';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function act_cell7()
    {
        $url = self::URL.'act_cell7';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function act_cell8()
    {
        $url = self::URL.'act_cell8';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function act_cell9()
    {
        $url = self::URL.'act_cell9';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function act_cell10()
    {
        $url = self::URL.'act_cell10';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function act_cell11()
    {
        $url = self::URL.'act_cell11';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    //

    //deactive cell 1-11
    static function dea_cell()
    {
        $url = self::URL.'dea_cell';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dea_cell2()
    {
        $url = self::URL.'dea_cell2';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dea_cell3()
    {
        $url = self::URL.'dea_cell3';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dea_cell4()
    {
        $url = self::URL.'dea_cell4';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dea_cell5()
    {
        $url = self::URL.'dea_cell5';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dea_cell6()
    {
        $url = self::URL.'dea_cell6';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dea_cell7()
    {
        $url = self::URL.'dea_cell7';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dea_cell8()
    {
        $url = self::URL.'dea_cell8';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dea_cell9()
    {
        $url = self::URL.'dea_cell9';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dea_cell10()
    {
        $url = self::URL.'dea_cell10';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dea_cell11()
    {
        $url = self::URL.'dea_cell11';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    //

    //License 1-11
     static function add_license()
    {
        $url = self::URL.'add_license';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

     static function add_license2()
    {
        $url = self::URL.'add_license2';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

     static function add_license3()
    {
        $url = self::URL.'add_license3';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

     static function add_license4()
    {
        $url = self::URL.'add_license4';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

     static function add_license5()
    {
        $url = self::URL.'add_license5';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

     static function add_license6()
    {
        $url = self::URL.'add_license6';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

     static function add_license7()
    {
        $url = self::URL.'add_license7';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

     static function add_license8()
    {
        $url = self::URL.'add_license8';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

     static function add_license9()
    {
        $url = self::URL.'add_license9';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

     static function add_license10()
    {
        $url = self::URL.'add_license10';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

     static function add_license11()
    {
        $url = self::URL.'add_license11';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }
    //

    //active license 1-11
     static function act_license()
    {
        $url = self::URL.'act_license';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

     static function act_license2()
    {
        $url = self::URL.'act_license2';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

     static function act_license3()
    {
        $url = self::URL.'act_license3';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

     static function act_license4()
    {
        $url = self::URL.'act_license4';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

     static function act_license5()
    {
        $url = self::URL.'act_license5';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

     static function act_license6()
    {
        $url = self::URL.'act_license6';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

     static function act_license7()
    {
        $url = self::URL.'act_license7';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

     static function act_license8()
    {
        $url = self::URL.'act_license8';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

     static function act_license9()
    {
        $url = self::URL.'act_license9';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

     static function act_license10()
    {
        $url = self::URL.'act_license10';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

     static function act_license11()
    {
        $url = self::URL.'act_license11';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }
    //

    //deactive license 1-11
    static function dea_license()
    {
        $url = self::URL.'dea_license';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

    static function dea_license2()
    {
        $url = self::URL.'dea_license2';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

    static function dea_license3()
    {
        $url = self::URL.'dea_license3';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

    static function dea_license4()
    {
        $url = self::URL.'dea_license4';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

    static function dea_license5()
    {
        $url = self::URL.'dea_license5';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

    static function dea_license6()
    {
        $url = self::URL.'dea_license6';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

    static function dea_license7()
    {
        $url = self::URL.'dea_license7';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

    static function dea_license8()
    {
        $url = self::URL.'dea_license8';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

    static function dea_license9()
    {
        $url = self::URL.'dea_license9';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

    static function dea_license10()
    {
        $url = self::URL.'dea_license10';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

    static function dea_license11()
    {
        $url = self::URL.'dea_license11';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }
    //

    //change2

    static function plan_remedy()
    {
        $url = self::URL.'plan_remedy';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }    
    static function plan_onair()
    {
        $url = self::URL.'plan_onair';
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
        }catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);

    }

}
